# Pre-commit template

Git에서 커밋 메시지를 작성하기 전에 호출되어 커밋하기 전 문법 오류나 스타일, 타입 오류 등을 확인할 수 있습니다.

pre-commit을 사용하려면 먼저 `pre-commit` 패키지 설치가 필요합니다.

```bash
pip install pre-commit
```

## pre-commit 적용 순서

1. 저장소 초기화

   ```bash
   git init
   ```

1. `pre-commit` 설치

   ```bash
   # repository 경로에서 실행
   pre-commit install
   ```

1. `.pre-commit-config.yaml` 작성

   > 이 저장소의 `.pre-commit-config.yaml` 파일은 [ultralytics](https://github.com/ultralytics/ultralytics/blob/main/.pre-commit-config.yaml)을 일부 수정하였습니다.

   여기서 제공하는 내용은 다음과 같습니다.

   - [black](https://black.readthedocs.io/en/stable/): 코드 스타일을 통일시켜 줍니다.
   - [isort](https://pycqa.github.io/isort/): import를 정렬합니다.
   - [mdformat](https://mdformat.rtfd.io/): Markdown 형식을 맞춰줍니다.
   - [flake8](https://flake8.pycqa.org/en/latest/): 오류 및 코드 스타일이 PEP8에 준하는지 검사합니다.
   - [codespell](https://github.com/codespell-project/codespell): 오탈자를 검사합니다.

1. 커밋 or pre-commit 실행

   ```bash
   # git commit -m "blah blah"
   ```

   수정할 내용이 있을 경우 아래와 같이 Failed 메시지가 출력됩니다.

   ```bash
   pre-commit run
   fix end of files.........................................................Failed
   - hook id: end-of-file-fixer
   - exit code: 1
   - files were modified by this hook

   Fixing README.md

   trim trailing whitespace.................................................Passed
   check for case conflicts.................................................Passed
   check docstring is first.............................(no files to check)Skipped
   detect private key.......................................................Passed
   Upgrade code.........................................(no files to check)Skipped
   Sort imports.........................................(no files to check)Skipped
   black................................................(no files to check)Skipped
   MD formatting............................................................Failed
   - hook id: mdformat
   - files were modified by this hook
   flake8...............................................(no files to check)Skipped
   codespell................................................................Passed
   ```
